if (typeof jQuery === 'undefined') {
    throw new Error('Bootstrap\'s JavaScript requires jQuery')
} + (function($) {

    $.fn.visible = function(partial) {

        var $t = $(this),
            $w = $(window),
            viewTop = $w.scrollTop(),
            viewBottom = viewTop + $w.height(),
            _top = $t.offset().top,
            _bottom = _top + $t.height(),
            compareTop = partial === true ? _bottom : _top,
            compareBottom = partial === true ? _top : _bottom;

        return ((compareBottom <= viewBottom) && (compareTop >= viewTop));

    };

})(jQuery) + (function($) {
    $.fn.itemHeightsEquals = function(add) {

        var items = $(this), //grab all slides
            heights = [], //create empty array to store height values
            tallest,
            additional = (add == "") ? parseInt(add) : 0; //create variable to make note of the tallest slide

        var normalizeHeights = function() {

            items.each(function() { //add heights to array
                heights.push($(this).height());
            });
            // console.log("height : "+heights)
            tallest = Math.max.apply(null, heights); //cache largest value
            // console.log(tallest+" "+additional);
            items.each(function() {
                $(this).css('min-height', (tallest + additional ) + 'px');
            });
        };

        normalizeHeights();

        $(window).on('resize orientationchange', function() {
            //reset vars
            tallest = 0;
            heights.length = 0;

            items.each(function() {
                $(this).css('min-height', '0'); //reset min-height
            });
            normalizeHeights(); //run it again 
        });

    }
})(jQuery) + (function($) {

var $event = $.event,
    $special,
    resizeTimeout;

$special = $event.special.debouncedresize = {
    setup: function() {
        $( this ).on( "resize", $special.handler );
    },
    teardown: function() {
        $( this ).off( "resize", $special.handler );
    },
    handler: function( event, execAsap ) {
        // Save the context
        var context = this,
            args = arguments,
            dispatch = function() {
                // set correct event type
                event.type = "debouncedresize";
                $event.dispatch.apply( context, args );
            };

        if ( resizeTimeout ) {
            clearTimeout( resizeTimeout );
        }

        execAsap ?
            dispatch() :
            resizeTimeout = setTimeout( dispatch, $special.threshold );
    },
    threshold: 500
};

})(jQuery);
!function($) {
    "use strict";
    var Shift = function(element) {
        this.$element = $(element);
        this.$prev = this.$element.prev();
        !this.$prev.length && (this.$parent = this.$element.parent())
    }
    Shift.prototype = {
        constructor: Shift,
        init: function() {
            var $el = this.$element,
                method = $el.data()['toggle'].split(':')[1],
                $target = $el.data('target')
            $el.hasClass('in') || $el[method]($target).addClass('in')
        },
        reset: function() {
            this.$parent && this.$parent['prepend'](this.$element);
            !this.$parent && this.$element['insertAfter'](this.$prev)
            this.$element.removeClass('in');
        }
    }
    $.fn.shift = function(option) {
        return this.each(function() {
            var $this = $(this),
                data = $this.data('shift')
            if (!data) $this.data('shift', (data = new Shift(this)))
            if (typeof option == 'string') data[option]()
        })
    }
    $.fn.shift.Constructor = Shift
}(window.jQuery);
Date.now = Date.now || function() {
    return +new Date;
};

(function($) {

    $(document).ready(function() {

        $.noConflict();

    	// masonry effex
    	var containerMason = $('.content-mason');

    	// containerMason.masonry({
    	// 	// itemSelector : '.bst-block',
    	// });

    	if(containerMason.length){

    		var linkBlock = $('.img-block');

    		// console.log(linkBlock)
    		linkBlock.hover(function() {

    				var $this = $(this);

                    var fromTop = ($('.img-wrapper', $this).height() / 2 - $('.caption-block', $this).height() / 2);
                    $('.caption-block', $this).css('margin-top', fromTop);

                    $('.mediaCaption', $this).height($('.img-wrapper', $this).height());

                    $('.mask', this).css('height', $('.img-wrapper', this).height());
                    $('.mask', this).css('width', $('.img-wrapper', this).width());
                    $('.mask', this).css('margin-top', $('.img-wrapper', this).height());


                    $('.mask', this).stop(1).show().css('margin-top', $('.img-wrapper', this).height()).animate({
                        marginTop: 0
                    }, 100, function() {

                        $('.caption-block', $this).css('display', 'block');
                        if (Modernizr.csstransitions) {
                            $('.caption-block a').addClass('animated');


                            $('.caption-block a', $this).removeClass('flipOutX');
                            $('.caption-block a', $this).addClass('fadeInDown');

                        } else {

                            $('.caption-block', $this).stop(true, false).fadeIn('fast');
                        }

                    });
    			
    		}, function() {
    				var $this = $(this);


                    $('.mask', this).stop(1).show().animate({
                        marginTop: $('.img-wrapper', $this).height()
                    }, 200, function() {

                        if (Modernizr.csstransitions) {
                            $('.caption-block a', $this).removeClass('fadeInDown');
                            $('.caption-block a', $this).addClass('flipOutX');

                        } else {
                            $('.caption-block', $this).stop(true, false).fadeOut('fast');
                        }

                    });
    		});
    	}

        if($('.coup-1').length){
            $('.coup-1').itemHeightsEquals();
        }

        // settings rooms list
        if(($('.list-rooms').length) && ($('.bst-block').length) ){

            var heightBlock = [];

            $('div[class^="bst-col-"]').each(function() { //add heights to array
                heightBlock.push($(this).height());
            });
            // console.log(heightBlock);

            tallest = Math.max.apply(null, heightBlock); //cache largest value

            roomHeight = tallest / 3;

            if($('.list-rooms').height() > roomHeight){
                $('.list-rooms').height(roomHeight).css({
                    'overflow': 'scroll',
                    'margin-bottom' : '50px'
                });;
            }
        }

        // Slider init
        if($().flexslider){
            $('.flexslider-single').flexslider({
                    animation: "slide",
                    controlNav: false,
                    slideshow: false,
            });
        }

        function initFlexModal() {
             $('.modal-flexslider').flexslider({
                    animation: "slide",
                    controlNav: false,
                    slideshow: false,
            });
        };

        if($().nanoScroller){
            $(".nano").nanoScroller();
        }

        // select2 bootstrap
        if($().select2){
            $(".select-custom").select2();
        }

        // set active list
        var mainNav = $("#main-nav"); 
            ulNav = mainNav.find('ul.nav'),
            activeNav = ulNav.find('li.active');

        if(mainNav.length){
            var angle = activeNav.width();// in radians

            $('body').append('<style>.navbar-nav > li.active:after, .navbar-nav > li.active:before{border-left-width: ' + angle + 'px;}</style>');
        }

        // modal 
        $('#detailModal').on('shown.bs.modal', function () {
            setTimeout(function(){
                    if($().flexslider){
                    initFlexModal();    
                    console.log('triggered');
                    }
                    if($().nanoScroller){
                        $(".nano").nanoScroller();
                    }
            }, 500);
        })

    });

    // Load is used to ensure all images have been loaded, impossible with document

    $(window).load(function () {

        // Takes the gutter width from the bottom margin of .post

        var gutter = parseInt($('.item-post').css('marginBottom'));
        var container = $('#posts');



        // Creates an instance of Masonry on #posts

        if($('.col-grid').length){
            container.masonry({
                gutter: gutter,
                itemSelector: '.item-post',
                columnWidth: '.item-post'
            });
        }
        
        
        
        // This code fires every time a user resizes the screen and only affects .post elements
        // whose parent class isn't .container. Triggers resize first so nothing looks weird.
        
        $(window).bind('resize', function () {
            if (!$('#posts').parent().hasClass('container')) {
                
                
                
                // Resets all widths to 'auto' to sterilize calculations
                
                post_width = $('.post').width() + gutter;
                $('#posts, body > .col-grid').css('width', 'auto');
                
                
                
                // Calculates how many .post elements will actually fit per row. Could this code be cleaner?
                
                posts_per_row = $('#posts').innerWidth() / post_width;
                floor_posts_width = (Math.floor(posts_per_row) * post_width) - gutter;
                ceil_posts_width = (Math.ceil(posts_per_row) * post_width) - gutter;
                posts_width = (ceil_posts_width > $('#posts').innerWidth()) ? floor_posts_width : ceil_posts_width;
                if (posts_width == $('.item-post').width()) {
                    posts_width = '100%';
                }
                
                
                
                // Ensures that all top-level elements have equal width and stay centered
                
                $('#posts, .col-grid').css('width', posts_width);
                $('.col-grid').css({'margin': '0 auto'});
                    
            
            
            }
        }).trigger('resize');
        


    });

})(jQuery);