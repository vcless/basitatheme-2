(function($) {

    $(document).ready(function() {

        //Create a function for tiles
        function basita_tiles_init() {

                //Fix Masonry container width error
                function basita_fix_masonry() {
                    var screenWidth = jQuery(window).width();
                    if (screenWidth < 960) {
                        jQuery('#mainpage-mos').width(screenWidth);
                    } else {
                        jQuery('#mainpage-mos').width(960);
                    }

                };
                basita_fix_masonry();
                jQuery(window).resize(basita_fix_masonry);

                //Masonry Settings
                $('.content-mason').masonry({
                    itemSelector: '.bst-block',
                    columnWidth: 160,
                    isAnimated: true,
                    isFitWidth: true
                });

                //Allow effects when hovering on tiles
                $('.bst-block').not('.exclude').hover(function() {
                        $('.bst-block').not(this).addClass('fade');
                    },
                    function() {
                        $('.bst-block').removeClass('fade');
                    });
                $('.bst-block').append('<img class="tilehover" src="images/tilehover.png" alt=" "/>');

                //Lightbox
                $("a.lightbox").click(function() {
                    $(this).next(".tile-pre").addClass("tempsrc");
                });

            } //end basita_tiles_init();

        // basita_tiles_init(); //run the function when page is ready

        // your code here
        $.noConflict();

        // homepage content slider 
        if($().flexslider){
            $('.flexslider').flexslider({
                    animation: "slide",
                    controlNav: false,
                    slideshow: false,
            });
        }

        if($().nanoScroller){
            $(".nano").nanoScroller();
        }
    });



})(jQuery)
